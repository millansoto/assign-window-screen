/*
    KWin script to switch the screen of all windows

    SPDX-FileCopyrightText: 2021 José Millán Soto <jmillan@kde-espana.org>

    SPDX-License-Identifier: GPL-2.0-or-later
 */

function assignAllWindowsScreen() {
    var allWindows = workspace.clientList();
    var defaultScreen = readConfig('defaultDisplay', 1) - 1;
    var config = [];
    config.push(readConfig('display1List', '').toString().split(','));
    config.push(readConfig('display2List', '').toString().split(','));
    config.push(readConfig('display3List', '').toString().split(','));
    config.push(readConfig('display4List', '').toString().split(','));
    for (var i = 0; i < allWindows.length; i++) {
        var client = allWindows[i];
        const clientName = client.resourceName.toString();
        var windowScreen = defaultScreen;
        for (var j = 0; j < 4; j++) {
            if (config[j].includes(clientName)) {
                windowScreen = j;
            }
        }
        if (windowScreen >= workspace.numScreens) {
            windowScreen = 0;
        }
        if (client.moveableAcrossScreens) {
            workspace.sendClientToScreen(client, windowScreen);
        }
    }
}

registerUserActionsMenu(function (client) {
  return {
    text: "Display client resource name",
    triggered: function (action) {
        const clientName = client.resourceName.toString();
        callDBus("org.freedesktop.Notifications", "/org/kde/osdService", "org.kde.osdService", "showText", "", clientName);
    }
  };
})

registerShortcut("assignAllWindowsScreen", "Set the screen of all windows", "Meta+Shift+K", assignAllWindowsScreen);
